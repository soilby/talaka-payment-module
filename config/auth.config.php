<?php

return array(
    'bjyauthorize' => [
        'guards' => [
            'BjyAuthorize\Guard\Route' => [
                // Resource
                ['route' => 'payment/notify', 'roles' => ['guest', 'user']],
                ['route' => 'payment/callback/success', 'roles' => ['guest', 'user']],
                ['route' => 'payment/callback/fail', 'roles' => ['guest', 'user']],
                ['route' => 'payment/form', 'roles' => ['user']],


            ]
        ]
    ]
);