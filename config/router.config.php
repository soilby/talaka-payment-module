<?php

return [
    'router' => [
        'routes' => [
            'payment' => [
                'type' => 'literal',
                'options' => [
                    'route' => '/payment',
                    'defaults' => [
                        'controller' => 'PaymentController',
                    ],
                ],
                'child_routes' => [
                    'form' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => '/form',
                            'defaults' => [
                                'controller' => 'PayFormController',
                                'action' => 'get-pay-form'
                            ],
                        ]
                    ],
                    'notify' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => '/notify/:gateway',
                            'defaults' => [
                                'controller' => 'PaymentController',
                                'action' => 'notify'
                            ],
                        ]
                    ],
                    'callback' => [
                        'type' => 'literal',
                        'options' => [
                            'route' => '/callback',
                            'defaults' => [
                                'controller' => 'CallbackController',
                            ],
                        ],
                        'child_routes' => [
                            'success' => [
                                'type' => 'segment',
                                'options' => [
                                    'route' => '/success/:gateway',
                                    'defaults' => [
                                        'action' => 'callback',
                                        'success' => true
                                            
                                    ],
                                ],
                            ],
                            'fail' => [
                                'type' => 'segment',
                                'options' => [
                                    'route' => '/fail/:gateway',
                                    'defaults' => [
                                        'action' => 'callback',
                                        'success' => false
                                    ],
                                ],
                            ]
                        ]
                    ],
                ],
            ],

        ]
    ]
];
