<?php

return [

    'service_manager' => [

        'alias' => [

        ],
        'invokables' => [

        ],
        'factories' => [
            'TalakaPaymentOrderService' => 'Talaka\Payment\Service\Factory\OrderServiceFactory'
        ]
    ],
    'controllers' => [
        'invokables' => [
        ],
        'factories' => [
            'PaymentController' => 'Talaka\Payment\Controller\Factory\PaymentControllerFactory',
            'PayFormController' => 'Talaka\Payment\Controller\Factory\PayFormControllerFactory',
            'CallbackController' => 'Talaka\Payment\Controller\Factory\CallbackControllerFactory'
        ]
    ],
    'view_helpers' => [
        'invokables' => [
        ]
    ]
];