<?php

namespace Talaka\Payment;

return array(
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    ), // doctrine

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'talaka_payment' => [
        'base_currency' => 'BYN',
        'currency_converter' => 'NbrbRateService',
        'order' => [
            'crowdfunding' => [
                'builder' => 'CrowdfundingOrderBuilder',
                'class' => 'Crowdfunding\Entity\Order',
                'callback' => [
                    'success' => [
                        'type' => 'forward',
                        'controller' => 'CrowdfundingPayFlowController',
                        'params' => [
                            'action' => 'callback'
                        ]
                    ],
                    'fail' => [
                        'type' => 'forward',
                        'controller' => 'CrowdfundingPayFlowController',
                        'params' => [
                            'action' => 'callback'
                        ]
                    ],
                ]
            ]
        ],
        'gateway' => [

        ]
    ]
);