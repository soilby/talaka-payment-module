<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 23.53
 */

namespace Talaka\Payment\Transaction;


use Talaka\Payment\Entity\Notification;
use Talaka\Payment\Entity\Transaction;
use Zend\Http\Request;

interface BuilderInterface {

    /**
     * @param $fields
     *
     * @return Notification
     */
    public function parseNotification($fields);

    /**
     * @param Request $request
     *
     * @return Notification
     */
    public function handleNotification(Request $request);

    /**
     * @param $transaction
     * @param Notification $parsedFields
     *
     * @return Transaction
     */
    public function updateTransaction(Transaction $transaction, Notification $parsedFields = null);


    /**
     * @param Request $request
     *
     * @return Notification
     */
    public function getTransaction(Request $request);
} 