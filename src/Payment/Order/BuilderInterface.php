<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 23.53
 */

namespace Talaka\Payment\Order;



use Talaka\Payment\Entity\Notification;
use Talaka\Payment\Entity\OrderInterface;

interface BuilderInterface {

    public function build(OrderInterface $order, $data);

}