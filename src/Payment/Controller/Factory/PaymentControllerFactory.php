<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 12.16
 */

namespace Talaka\Payment\Controller\Factory;


use Talaka\Payment\Controller\CallbackController;
use Talaka\Payment\Controller\PayFormController;
use Talaka\Payment\Controller\PaymentController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PaymentControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator)  {
        $sm = $serviceLocator->getServiceLocator();

        $paymentApi = $sm->get('WebPayPaymentApi');
        $orderService = $sm->get('TalakaPaymentOrderService');

//        $router = $sm->get('router');

//        $config = $sm->get('config');
//        $moduleConfig = $config['talaka_payment'];

//        $orderConfig = array_key_exists('order', $moduleConfig) ? $moduleConfig['order'] : [];

        $controller = new PaymentController($paymentApi, $orderService);

        return $controller;
    }
} 