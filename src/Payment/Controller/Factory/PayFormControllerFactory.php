<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 12.16
 */

namespace Talaka\Payment\Controller\Factory;


use Talaka\Payment\Controller\PayFormController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PayFormControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator)  {
        $sm = $serviceLocator->getServiceLocator();

        $orderService = $sm->get('TalakaPaymentOrderService');

        $controller = new PayFormController($orderService);

        return $controller;
    }
} 