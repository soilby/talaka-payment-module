<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 12.16
 */

namespace Talaka\Payment\Controller\Factory;


use Talaka\Payment\Controller\CallbackController;
use Talaka\Payment\Controller\PayFormController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CallbackControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator)  {
        $sm = $serviceLocator->getServiceLocator();

        $paymentApi = $sm->get('WebPayPaymentApi');
        $orderService = $sm->get('TalakaPaymentOrderService');

        $router = $sm->get('router');

        $config = $sm->get('config');
        $moduleConfig = $config['talaka_payment'];

        $orderConfig = array_key_exists('order', $moduleConfig) ? $moduleConfig['order'] : [];

        $handlersMap = [];
        foreach ($orderConfig as $type => $orderOptions) {
            $callbackConf = $orderOptions['callback'];

            foreach ($callbackConf as $case => $handler) {
                if (is_string($handler)) {
                    if (strpos($handler, 'http') === false) {
                        //is route
                        $handler = $router->assemble([], ['name' => $handler]);

                    }
                }

                if (!array_key_exists($type, $handlersMap)) $handlersMap[$type] = [];

                $handlersMap[$type][$case] = $handler;

            }


        }

        $controller = new CallbackController($paymentApi, $orderService, $handlersMap);

        return $controller;
    }
} 