<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 12.10
 */

namespace Talaka\Payment\Controller;


use Crowdfunding\Service\CampaignService;
use Talaka\Payment\Service\OrderService;
use Talaka\Service\SocialService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class PayFormController extends AbstractActionController {


    /**
     * @var OrderService
     */
    protected $orderService;

    public function __construct($orderService)   {
        $this->orderService = $orderService;
    }

    public function getPayFormAction()  {
        try {
            $auth = $this->getAuthService();
            $user = $auth->getIdentity();

            $requestContent = $this->request->getContent();
            $request = json_decode($requestContent, true);

            if (!$request)  {
                throw new \Exception('Request malformed. Should contain gateway, type, target fields in JSON notation.');
            }

            $gateway = array_key_exists('gateway', $request) ? $request['gateway'] : 'webpay';
            $type = array_key_exists('type', $request) ? $request['type'] : 'default';
            $target = array_key_exists('target', $request) ? $request['target'] : [];

            $order = $this->orderService->factory($type, $gateway, $target, $user);

            $this->orderService->save($order);
            $this->orderService->getDM()->flush();

            $viewModel = new ViewModel();

            $template = 'payment/form/' . $gateway;

            $viewModel->setTemplate($template);

            $viewModel->setVariable('order', $order);

        }
        catch (\Exception $e)   {
            $viewModel = new JsonModel([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        return $viewModel;

    }

    protected function getAuthService()    {
        return $this->getServiceLocator()->get('zfcuser_auth_service');
    }
} 