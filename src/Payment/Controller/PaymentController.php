<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 5.3.15
 * Time: 21.18
 */

namespace Talaka\Payment\Controller;


use Application\Controller\AbstractController;
use Talaka\Payment\Service\OrderService;
use Talaka\WebPayIntegration\Api\PaymentApi;
use Zend\View\Model\ViewModel;

class PaymentController extends AbstractController {

    /**
     * @var PaymentApi
     */
    protected $paymentApi;

    /**
     * @var OrderService
     */
    protected $orderService;

    public function __construct($paymentApi, $orderService)    {
        $this->paymentApi = $paymentApi;
        $this->orderService = $orderService;
    }

    public function notifyAction()   {
        try {
            $gateway = $this->params()->fromRoute('gateway');

            $content = $this->request->getContent();
            
            ob_start();
            var_dump($content);
            $s = ob_get_clean();

            file_put_contents('/tmp/payment_' . $gateway, date('Y-m-d H:i:s') . ' ' . $s . PHP_EOL, FILE_APPEND);


            $this->orderService->handleNotification($gateway, $this->getRequest());

            $this->orderService->getDM()->flush();

            $response = $this->getResponse();
            if ($gateway === 'webpay')  {
                $response->setContent('OK');
            }

            $response->setStatusCode(200);

            return $response;
        }
        catch(\Exception $e)    {
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);

            $response = $this->getResponse();
            $response->setContent('Not ok ' . $e->getMessage() . PHP_EOL . (string)$e);
            $response->setStatusCode(500);

            return $response;
        }




//        batch_timestamp=1429233189
//        currency_id=BYR
//        amount=200000
//        payment_method=test
//        order_id=26596
//        site_order_id=55305e23a6369f6c698b4574
//        transaction_id=811011289
//        payment_type=4
//        rrn=142923324016
//        wsb_signature=
//        action=0
//        rc=W0269%2800%29
//        approval=000000


    }
} 