<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 17.3.15
 * Time: 15.29
 */

namespace Talaka\Payment\Controller;


use Application\Controller\AbstractController;
use Talaka\Payment\Entity\OrderAbstract;
use Talaka\Payment\Entity\OrderInterface;
use Talaka\Payment\Service\OrderService;
use Talaka\WebPayIntegration\Api\PaymentApi;
use Zend\Http\Response;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class CallbackController extends AbstractController {

    /**
     * @var PaymentApi
     */
    protected $paymentApi;

    /**
     * @var OrderService
     */
    protected $orderService;


    protected $handlersMap;

    public function __construct($paymentApi, $orderService, $handlersMap)    {
        $this->paymentApi = $paymentApi;
        $this->orderService = $orderService;
        $this->handlersMap = $handlersMap;
    }

    public function callbackAction() {
        try {
            $gateway = $this->params()->fromRoute('gateway');
            $success = $this->params()->fromRoute('success');
            //        $orderId = $this->params()->fromQuery('wsb_order_num');
            //        $foreignTransactionId = $this->params()->fromQuery('wsb_tid');
            $request = $this->getRequest();

            $transaction = $this->orderService->handleCallback($gateway, $request, $success);

            $this->orderService->getDM()->flush();

            $order = $transaction->getOrder();

            $session = new Container('talaka_payment_order_callback');
            $session->offsetSet('order', $order);
            $session->offsetSet('transaction', $transaction);

            $handler = $this->getCallbackHandlersForOrder($order);

            switch (true) {
                case is_string($handler):
                    return $this->redirect()->toUrl($handler);

                case is_array($handler) && $handler['type'] === 'forward':
                    return $this->forward()->dispatch($handler['controller'], $handler['params']);

                default:
                    $success = $transaction->getStatus() === OrderAbstract::STATUS_CONFIRMED ||
                        $transaction->getStatus() === OrderAbstract::STATUS_PLACED;

                    return new ViewModel([
                        'success' => $success,
                        'order' => $order,
                        'transaction' => $transaction
                    ]);

            }
        }
        catch (\Exception $e)  {
            $ret = $this->orderService->processCallbackException($e);
            if ($ret instanceof \Exception) {
                throw $ret;
            }

            if ($ret instanceof Response)   {
                return $ret;
            }

            throw $e;
        }
    }

    protected function getCallbackHandlersForOrder(OrderInterface $order) {
        $orderType = $this->orderService->getTypeOfOrder($order);

        if (array_key_exists($orderType, $this->handlersMap)) {
            $handlers = $this->handlersMap[$orderType];

            switch ($order->getStatus())    {
                case OrderAbstract::STATUS_CONFIRMED:
                case OrderAbstract::STATUS_PLACED:
                    $handler = $handlers['success'];
                    break;

                default:
                    $handler = $handlers['fail'];

            }

            return $handler;
        }

        return [];
    }
}