<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 22.51
 */

namespace Talaka\Payment\Service\Factory;


use Talaka\Payment\Service\OrderService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class OrderServiceFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator)  {

        $dm = $serviceLocator->get('doctrine.documentmanager.odm_default');

        $config = $serviceLocator->get('config');
        $moduleConfig = $config['talaka_payment'];

        $orderService = new OrderService($moduleConfig, $dm);

        $currencyConverterServiceId = array_key_exists('currency_converter', $moduleConfig) ? $moduleConfig['currency_converter'] : null;
        if ($currencyConverterServiceId) {
            if (!$serviceLocator->has($currencyConverterServiceId))   {
                throw new \Exception("Talaka\\PaymentModule configured with invalid currency_convertor `$currencyConverterServiceId`");
            }
            $currencyConverter = $serviceLocator->get($currencyConverterServiceId);

            $orderService->setCurrencyConverter($currencyConverter);
        }


        $gatewayConfig = array_key_exists('gateway', $moduleConfig) ? $moduleConfig['gateway'] : [];
        if (!$gatewayConfig)   {
            throw new \Exception('Talaka\Payment module setup without no gateway');
        }

        foreach ($gatewayConfig as $gatewayName => $gatewayOptions)  {
            $serviceId = $gatewayOptions['transaction_builder'];
            if ($serviceLocator->has($serviceId)) {
                $builder = $serviceLocator->get($serviceId);
            }
            else    {
                throw new \Exception("Talaka\\Payment configured with missing transaction builder service $serviceId");
            }

            $orderService->addTransactionBuilder($gatewayName, $builder);

        }



        $orderClasses = [];

        $orderConfig = array_key_exists('order', $moduleConfig) ? $moduleConfig['order'] : [];

        if (!array_key_exists('default', $orderConfig)) {
            $orderConfig['default'] = [
                'class' => 'Talaka\Payment\Entity\DefaultOrder'
            ];
        }

        foreach ($orderConfig as $orderType => $orderOptions)  {
            if (array_key_exists('builder', $orderOptions)) {
                $serviceId = $orderOptions['builder'];
                if ($serviceLocator->has($serviceId)) {
                    $builder = $serviceLocator->get($serviceId);
                }
                else    {
                    throw new \Exception("Talaka\\Payment configured with missing order builder service $serviceId");
                }

                $orderService->addOrderBuilder($orderType, $builder);

                $orderService->addDiscriminatorMap($orderType, $orderOptions['class']);
            }

            $orderClass = $orderOptions['class'];
            $orderClasses[$orderType] = $orderClass;

        }

        $orderService->setOrderClasses($orderClasses);

        return $orderService;
    }
} 