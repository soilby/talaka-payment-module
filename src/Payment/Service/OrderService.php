<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 22.40
 */

namespace Talaka\Payment\Service;


use Doctrine\ODM\MongoDB\DocumentManager;
use Payment\Service\Exception\OrderMissingException;
use Talaka\ExchangeCurrencyModule\Service\ExchangeCurrencyServiceInterface;
use Talaka\Payment\Entity\DefaultOrder;
use Talaka\Payment\Entity\Notification;
use Talaka\Payment\Entity\Order;
use Talaka\Payment\Entity\OrderAbstract;
use Talaka\Payment\Entity\Transaction;
use Talaka\Payment\Order\BuilderInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\Http\Request;
use Zend\Http\Response;

class OrderService {
    use EventManagerAwareTrait;

    const HANDLE_PAYMENT_CALLBACK_EVENT = 'handle-payment-callback-event';

    /**
     * @var DocumentManager
     */
    protected $dm;

    protected $baseCurrency;

    public $supportedGateway;

    protected $orderClasses;

    /**
     * @var BuilderInterface[]
     */
    public $orderBuilders = [];

    /**
     * @var ExchangeCurrencyServiceInterface
     */
    protected $currencyConverter;


    /**
     * @var \Talaka\Payment\Transaction\BuilderInterface[]
     */
    public $transactionBuilders = [];

    public function __construct($config, $dm)   {
        $this->supportedGateway = array_key_exists('gateway', $config) ? $config['gateway'] : ['webpay' => null];
        $this->baseCurrency = array_key_exists('base_currency', $config) ? $config['base_currency'] : 'BYN';

        $this->dm = $dm;
    }

    public function addOrderBuilder($type, $builder)    {
        $this->orderBuilders[$type] = $builder;
    }

    public function addTransactionBuilder($gateway, $builder)   {
        $this->transactionBuilders[$gateway] = $builder;
    }

    public function addDiscriminatorMap($type, $class)  {
        $classMetaData = $this->dm->getClassMetadata('Talaka\Payment\Entity\OrderAbstract');
        $classMetaData->discriminatorMap[$type] = $class;
    }

    /**
     * @param $type
     * @return BuilderInterface
     * @throws \Exception
     */
    protected function getOrderBuilder($type)   {
        if (!array_key_exists($type, $this->orderBuilders)) {
            return null;
        }

        return $this->orderBuilders[$type];
    }

    /**
     * @param $gateway
     * @return \Talaka\Payment\Transaction\BuilderInterface
     * @throws \Exception
     */
    protected function getTransactionBuilder($gateway)   {
        if (!array_key_exists($gateway, $this->transactionBuilders)) {
            return null;
        }

        return $this->transactionBuilders[$gateway];
    }



    public function factory($type, $gateway, $target, $user)   {
        if (!array_key_exists($type, $this->orderClasses)) {
            throw new \Exception("Order of type `$type` is not configured");
        }

        if (!array_key_exists($gateway, $this->supportedGateway)) {
            throw new \Exception("Gateway $gateway isn't available");
        }

        $orderClass = $this->orderClasses[$type];
        $order = $this->instantiateOrder($orderClass);
        $order->setGateway($gateway);
        $order->setUser($user);

        $orderBuilder = $this->getOrderBuilder($type);

        if ($orderBuilder && $target) {
            $orderBuilder->build($order, $target);
        }

        $gatewayOptions = $this->supportedGateway[$gateway];

        $chargeCurrency = array_key_exists('charge_currency', $gatewayOptions) ? $gatewayOptions['charge_currency'] : $this->baseCurrency;

        $order->setBaseCurrency($this->baseCurrency);
        $order->setChargeCurrency($chargeCurrency);


        if ($this->baseCurrency === $chargeCurrency)    {
            $rate = 1;
        }
        else    {
            if (!$this->currencyConverter)   {
                throw new \Exception("Currency converter didn't setup for Talaka\\Payment module but intent to used");
            }
            $rate = $this->currencyConverter->getRateFor($this->baseCurrency, $chargeCurrency);
        }
        $order->setConversionRate($rate);

        $chargePrice = round($order->getPrice() / $rate, 2);

        $order->setChargePrice($chargePrice);


        return $order;
    }

    /**
     * @param $orderClass
     *
     * @throws \Exception
     * @return OrderAbstract
     */
    protected function instantiateOrder($orderClass)   {
        if (!class_exists($orderClass))  {
            throw new \Exception("Class $orderClass not exist");
        }

        return new $orderClass();
    }

    public function save($entity)  {
        $this->dm->persist($entity);
    }

    public function getDM() {
        return $this->dm;
    }

    protected function getRepository($class = 'Talaka\Payment\Entity\OrderAbstract')    {
        return $this->dm->getRepository($class);
    }


    /**
     * @param mixed $orderClasses
     */
    public function setOrderClasses($orderClasses)
    {
        $this->orderClasses = $orderClasses;
    }

    /**
     * @param $id
     *
     * @return OrderAbstract
     *
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @throws \Doctrine\ODM\MongoDB\Mapping\MappingException
     */
    public function getById($id)    {
        return $this->getRepository()->find($id);
    }


    public function getTypeOfOrder($order)  {
        $metaData = $this->getDM()->getClassMetadata(get_class($order));

        return $metaData->discriminatorValue;
    }


    public function factoryTransaction($gateway)   {
        $transaction = new Transaction();
        $transaction->setGateway($gateway);

        return $transaction;
    }

    protected function updateTransaction(Transaction $transaction, Notification $parsedFields, $isSuccess = null)  {
        $transaction->setForeignTransactionId($parsedFields->foreignTransactionId);

        $transaction->setDate($parsedFields->date);

        $transaction->setCurrency($parsedFields->currency);

        $transaction->setAmount($parsedFields->amount);

        $transaction->setForeignOrderId($parsedFields->foreignOrderId);


        $transaction->setOrigin(get_object_vars($parsedFields));

        /**
         * FIXME: Rewrite 
         */
        if (!$transaction->getStatus() && !is_null($isSuccess)) {
            $status = $isSuccess ? OrderAbstract::STATUS_PLACED : OrderAbstract::STATUS_CANCELED;
            $transaction->setStatus($status);
        }

        return $transaction;
    }

    public function handleCallback($gateway, Request $request, $success)    {
        $builder = $this->getTransactionBuilder($gateway);

        $notification = $builder->getTransaction($request);


        $orderId = $notification->orderId;

        $order = $this->getById($orderId);
        if (!$order) {
            throw new OrderMissingException();
        }


        if (
            $order->getStatus() === OrderAbstract::STATUS_CONFIRMED ||
            $order->getStatus() === OrderAbstract::STATUS_PLACED
        )   {
            //do nothing
            $transactions = $order->getTransactions();
            $transaction = $transactions->last();
        }
        else {

            $transaction = null;
            if ($notification->foreignTransactionId) {
                $foreignTransactionId = $notification->foreignTransactionId;
                $transaction = $this->getRepository('Talaka\Payment\Entity\Transaction')->findOneBy([
                    'foreignTransactionId' => $foreignTransactionId
                ]);
            }

            if (!$transaction) {
                $transaction = $this->factoryTransaction($gateway);

                $transaction->setOrder($order);
                $order->getTransactions()->add($transaction);

                $this->getDM()->persist($transaction);
            }

            $this->updateTransaction($transaction, $notification, $success);

            $builder->updateTransaction($transaction, $notification, $order);

            $order->setForeignOrderId($transaction->getForeignOrderId());

            //update order status
            $order->setStatus($transaction->getStatus());

            $this->getEventManager()->trigger(self::HANDLE_PAYMENT_CALLBACK_EVENT, $this, [
                'orderStatus' => $order->getStatus(),
                'order' => $order,
                'transaction' => $transaction
            ]);

            $this->save($order);
        }

        return $transaction;
    }

    public function processCallbackException(\Exception $e)   {
        return $e;
    }



    public function handleNotification($gateway, Request $request)    {
        $builder = $this->getTransactionBuilder($gateway);
        $notification = $builder->handleNotification($request);

//        ob_start();
//        var_dump($notification);
//        $s = ob_get_clean();
//        file_put_contents('/tmp/payment2', date('Y-m-d H:i:s') . ' ' . $s . PHP_EOL, FILE_APPEND);

        $order = $this->getById($notification->orderId);

        $foreignTransactionId = $notification->foreignTransactionId;
        $transaction = $this->getRepository('Talaka\Payment\Entity\Transaction')->findOneBy([
            'foreignTransactionId' => $foreignTransactionId
        ]);

        if (!$transaction)  {
            $transaction = $this->factoryTransaction($gateway);


            $transaction->setOrder($order);
            $order->getTransactions()->add($transaction);

            $this->getDM()->persist($transaction);
        }

        $this->updateTransaction($transaction, $notification);
        $builder->updateTransaction($transaction, $notification);

        //update order status
        $order->setStatus($transaction->getStatus());

        $this->getEventManager()->trigger(self::HANDLE_PAYMENT_CALLBACK_EVENT, $this, [
            'orderStatus' => $order->getStatus(),
            'order' => $order,
            'transaction' => $transaction
        ]);

    }

    /**
     * @param ExchangeCurrencyServiceInterface $currencyConverter
     */
    public function setCurrencyConverter($currencyConverter)
    {
        $this->currencyConverter = $currencyConverter;
    }



}
