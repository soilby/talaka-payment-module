<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 21.44
 */

namespace Talaka\Payment\Entity;

use Crowdfunding\Entity\Order;

use Doctrine\Common\Collections\ArrayCollection;
use User\Entity\User;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Order
 * @package Talaka\Payment\Entity
 *
 * @ODM\Document(collection="order")
 * @ODM\InheritanceType("SINGLE_COLLECTION")
 * @ODM\DiscriminatorField(fieldName="type")
 * @ODM\DiscriminatorMap({"default"="DefaultOrder"})
 *
 */
class OrderAbstract implements OrderInterface {

    const STATUS_NEW = 'new';
    const STATUS_PLACED = 'placed';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_CANCELED = 'canceled';
    const STATUS_REFUNDED = 'refunded';


    /**
     * @var integer
     * @ODM\Id(strategy="increment")
     */
    protected $id;

    /**
     * @var float
     * @ODM\Float
     */
    protected $price;

    /**
     * @var string
     * @ODM\String
     */
    protected $baseCurrency;

    /**
     * @var string
     * @ODM\String
     */
    protected $chargeCurrency;

    /**
     * @var float
     * @ODM\Float
     */
    protected $conversionRate;

    /**
     * @var float
     * @ODM\Float
     */
    protected $chargePrice;

    /**
     * @var string
     * @ODM\String
     */
    protected $title;

    /**
     * @var string
     * @ODM\String
     */
    protected $titleCompatible;

    /**
     * @var \DateTime
     * @ODM\Date
     */
    protected $creationDate;

    /**
     * @var string
     * @ODM\String
     */
    protected $userId;


    /**
     * @var User
     */
    protected $user;

    /**
     * @var string
     * @ODM\String
     */
    protected $gateway;

    /**
     * @var string
     * @ODM\String
     */
    protected $status;

    /**
     * @var ArrayCollection
     * @ODM\ReferenceMany(targetDocument="\Talaka\Payment\Entity\Transaction", cascade={"all"})
     */
    protected $transactions;

    /**
     * @var string
     * @ODM\String
     */
    protected $foreignOrderId;


    /**
     * @var Item[]
     * @ODM\EmbedMany
     */
    protected $items = [];

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $sum
     */
    public function setPrice($sum)
    {
        $this->price = $sum;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
        $this->userId = $user->getId();
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * @param string $gateway
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return ArrayCollection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @param ArrayCollection $transactions
     */
    public function setTransactions($transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitleCompatible()
    {
        return $this->titleCompatible;
    }

    /**
     * @param string $titleCompatible
     */
    public function setTitleCompatible($titleCompatible)
    {
        $this->titleCompatible = $titleCompatible;
    }




    public function __construct()   {
        $this->creationDate = new \DateTime();
        $this->transactions = new ArrayCollection();
        $this->status = self::STATUS_NEW;
    }

    /**
     * @return string
     */
    public function getForeignOrderId()
    {
        return $this->foreignOrderId;
    }

    /**
     * @param string $foreignOrderId
     */
    public function setForeignOrderId($foreignOrderId)
    {
        $this->foreignOrderId = $foreignOrderId;
    }

    /**
     * @return string
     */
    public function getBaseCurrency()
    {
        return $this->baseCurrency;
    }

    /**
     * @param string $baseCurrency
     */
    public function setBaseCurrency($baseCurrency)
    {
        $this->baseCurrency = $baseCurrency;
    }

    /**
     * @return string
     */
    public function getChargeCurrency()
    {
        return $this->chargeCurrency;
    }

    /**
     * @param string $chargeCurrency
     */
    public function setChargeCurrency($chargeCurrency)
    {
        $this->chargeCurrency = $chargeCurrency;
    }

    /**
     * @return float
     */
    public function getChargePrice()
    {
        return $this->chargePrice;
    }

    /**
     * @param float $chargePrice
     */
    public function setChargePrice($chargePrice)
    {
        $this->chargePrice = $chargePrice;
    }

    /**
     * @return float
     */
    public function getConversionRate()
    {
        return $this->conversionRate;
    }

    /**
     * @param float $conversionRate
     */
    public function setConversionRate($conversionRate)
    {
        $this->conversionRate = $conversionRate;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item[] $items
     * @return OrderAbstract
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }


    public function addItem(Item $item) {
        $this->items[] = $item;
        
        return $this;
    }


} 