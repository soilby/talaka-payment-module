<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.3.15
 * Time: 22.29
 */

namespace Talaka\Payment\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Transaction
 * @package Payment\Entity
 *
 * @ODM\Document(collection="transaction")
 */
class Transaction {

    /**
     * @var string
     * @ODM\Id
     */
    protected $id;

    /**
     * @var \DateTime
     * @ODM\Date
     */
    protected $date;

    /**
     * @var string
     * @ODM\String
     */
    protected $gateway;


    /**
     * @var string
     * @ODM\String
     */
    protected $currency;

    /**
     * @var string
     * @ODM\Float
     */
    protected $amount;


    /**
     * @var string
     * @ODM\String
     */
    protected $foreignTransactionId;


    /**
     * @var string
     * @ODM\String
     */
    protected $foreignOrderId;


    /**
     * @var string
     * @ODM\String
     */
    protected $status;

    /**
     * @var OrderAbstract
     * @ODM\ReferenceOne(targetDocument="\Talaka\Payment\Entity\OrderAbstract", cascade={"all"})
     */
    protected $order;

    /**
     * @var array
     * @ODM\Hash
     */
    protected $origin;

    public function __construct()   {
        $this->date = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }



    /**
     * @return string
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * @param string $gateway
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * @return OrderAbstract
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param OrderAbstract $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getForeignTransactionId()
    {
        return $this->foreignTransactionId;
    }

    /**
     * @param string $foreignId
     */
    public function setForeignTransactionId($foreignId)
    {
        $this->foreignTransactionId = $foreignId;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getForeignOrderId()
    {
        return $this->foreignOrderId;
    }

    /**
     * @param string $foreignOrderId
     */
    public function setForeignOrderId($foreignOrderId)
    {
        $this->foreignOrderId = $foreignOrderId;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param array $origin
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}