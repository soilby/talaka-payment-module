<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 19.4.15
 * Time: 0.04
 */

namespace Talaka\Payment\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use User\Entity\User;

interface OrderInterface {
    /**
     * @return \DateTime
     */
    public function getCreationDate();

    /**
     * @return string
     */
    public function getId();


    /**
     * @return float
     */
    public function getPrice();

    /**
     * @return User
     */
    public function getUser();


    /**
     * @return string
     */
    public function getUserId();


    /**
     * @return string
     */
    public function getGateway();


    /**
     * @return string
     */
    public function getStatus();


    /**
     * @return ArrayCollection
     */
    public function getTransactions();


    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getTitleCompatible();


    /**
     * @return string
     */
    public function getForeignOrderId();

}