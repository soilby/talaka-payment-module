<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 20.4.15
 * Time: 23.59
 */

namespace Talaka\Payment\Entity;


class Notification {


    public $foreignTransactionId;

    public $orderId;
    public $foreignOrderId;

    public $currency;
    public $amount;

    public $status;

    public $date;

    public $transactionType;

    public $origin;

    public $signature;

//
//transaction_id
//• batch_timestamp
//• currency_id
//• amount
//• payment_method
//• payment_type
//• order_id
//• rrn
//• "Secret Key"
//
//
//
//batch_timestamp
//• currency_id
//• amount
//• payment_method
//• order_id
//• site_order_id
//• transaction_id
//• payment_type
//• rrn
//• "Secret Key"

} 