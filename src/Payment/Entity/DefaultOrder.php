<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 29.3.15
 * Time: 0.18
 */

namespace Talaka\Payment\Entity;


use Talaka\Payment\Entity\OrderAbstract;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class DefaultOrder
 * @package Talaka\Payment\Entity
 *
 * @ODM\Document()
 */
class DefaultOrder extends OrderAbstract {

} 