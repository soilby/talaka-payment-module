<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 9.6.16
 * Time: 1.34
 */

namespace Talaka\Payment\Entity;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Item
 * @package Talaka\Payment\Entity
 * @ODM\EmbeddedDocument
 */
class Item
{
    /**
     * @var string
     * @ODM\Field(type="string")
     */
    protected $contribId;
    
    /**
     * @var float
     * @ODM\Field(type="float")
     */
    protected $sum;
    
    /**
     * @var string
     * @ODM\Field(type="string")
     */
    protected $title;
    
    /**
     * @var string
     * @ODM\Field(type="string")
     */
    protected $promiseId;

    /**
     * @return string
     */
    public function getContribId()
    {
        return $this->contribId;
    }

    /**
     * @param string $contribId
     * @return Item
     */
    public function setContribId($contribId)
    {
        $this->contribId = $contribId;
        return $this;
    }

    /**
     * @return float
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param float $sum
     * @return Item
     */
    public function setSum($sum)
    {
        $this->sum = $sum;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Item
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getPromiseId()
    {
        return $this->promiseId;
    }

    /**
     * @param string $promiseId
     */
    public function setPromiseId($promiseId)
    {
        $this->promiseId = $promiseId;
    }
    
 
    
    

}